#!/bin/sh

VERSION=3343
SVNREV=3400

svn co -r${SVNREV} https://fteqw.svn.sourceforge.net/svnroot/fteqw/trunk/engine/qclib \
	fteqcc-${VERSION}+svn${SVNREV}

# Remove anything we're not interested in
find fteqcc-${VERSION}+svn${SVNREV} -type d -iname '.svn' -exec rm -rf "{}" \;

tar -czf fteqcc_${VERSION}+svn${SVNREV}.orig.tar.gz fteqcc-${VERSION}+svn${SVNREV}
#rm -rf fteqcc-${VERSION}+svn${SVNREV}
